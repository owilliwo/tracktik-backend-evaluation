# Tracktik PHP evaluation

## Repository structure

Ernesto might recognizes it quickly, it is based on **Docker4PHP**.

* *public/* directory contains PHP code
* *resources/* contains PDF file describing the exercise
* other files and directories are for local settings purpose

Just clone the repository (or unzip the archive), move to the project root and run this command:
```sh
make up
```

If `localhost` is already pointing to `127.0.0.1` (check in your **hosts** file), you should be OK. If not, just add this line to your **hosts** file:
```
127.0.0.1	tracktik-evaluation.localhost
```

Just access this URL: http://tracktik-evaluation.localhost:9876/

## Explanations

Products might exist, but prices are not for real.

There is a big issue with the method getSortedItems() provided in ElectronicItems class.
There is no way to get two items with the same price.
I had it rewritten.

I've moved Wired property (and its member functions) to Controller class as it's not a global one.

No need of a constructor for ElectronicItems (cart), as I provide addItem public method to fill it.

I did not use a rendering tool, just basic HTML direct printing.

I choose to add controllers in the cart, as they have their own prices, just as other items.
Before adding those to the cart, I control if you have not reached the maximum.

I've quickly written a basic autoloader, so I don't have to require classes. Also, it's quite a simple app, so I did not provide any namespace.

I did not provide a setter for the cart totalAmount property, as it's updated when adding an item to the cart.
