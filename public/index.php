<?php

// Autoloader.
require 'autoload.php';

// Cart management.
$cart = new ElectronicItems();

// Create 1 console with its 4 extras.
// Check for maximum extra before adding it to the cart.
$console = new Console('Playstation 3', 499.95);
$cart->addItem($console);
try {
	$remote_01 = new Controller('Dualshock 3W', 59.95, true);
	$console->addExtra($remote_01);
	$cart->addItem($remote_01);
} catch (Exception $e) {
	print $e->getMessage();
}
try {
	$remote_02 = new Controller('Dualshock 3W',59.95, true);
	$console->addExtra($remote_02);
	$cart->addItem($remote_02);
} catch (Exception $e) {
	print $e->getMessage();
}
try {
	$wired_01 = new Controller('Dualshock 3F',39.95, false);
	$console->addExtra($wired_01);
	$cart->addItem($wired_01);
} catch (Exception $e) {
	print $e->getMessage();
}
try {
	$wired_02 = new Controller('Dualshock 3F',39.95, false);
	$console->addExtra($wired_02);
	$cart->addItem($wired_02);
} catch (Exception $e) {
	print $e->getMessage();
}

// Create first TV with its extras.
// Check for maximum extra before adding it to the cart.
$tv_01 = new TV('Sony KB21',299.95);
$cart->addItem($tv_01);
try {
	$remote_tv_01 = new Controller('Sony KB21 Remote', 29.95, true);
	$tv_01->addExtra($remote_tv_01);
	$cart->addItem($remote_tv_01);
} catch (Exception $e) {
	print $e->getMessage();
}

// Create second TV with its extras.
// Check for maximum extra before adding it to the cart.
$tv_02 = new TV('Sony KX21',899.95);
$cart->addItem($tv_02);
$remote_tv_03 = new Controller('Sony KB21 Remote',29.95, true);
try {
	$remote_tv_02 = new Controller('Sony KB21 Remote',29.95, true);
	$tv_02->addExtra($remote_tv_02);
	$cart->addItem($remote_tv_02);
} catch (Exception $e) {
	print $e->getMessage();
}
try {
	$tv_02->addExtra($remote_tv_03);
	$cart->addItem($remote_tv_03);
} catch (Exception $e) {
	print $e->getMessage();
}

// Create microwave.
// No extras allowed.
$microwave = new Microwave('Samsung MW2103',129.95);
$cart->addItem($microwave);

// Render question 1.
print '<h2>Question 1: the sorted cart and its total amount</h2>';

// Render sorted items from cart.
$sortedItems = $cart->getSortedItems();
print '<ul>';
foreach ($sortedItems as $item) {
	print '<li>' . $item . '</li>';
}
print '</ul>';

// Render cart amount.
print 'Total: $' . $cart->getTotalAmount();

// Render question 2.
print '<h2>Question 2: the price for the console and its extras.</h2>';

// Get console(s) from cart, but consider only the first to return its price, as exercise is not very explicit on that point.
$cartConsole = $cart->getItemsByType('console');
print 'Console (with extras) price: $' . $cartConsole[0]->getTotalPrice();
