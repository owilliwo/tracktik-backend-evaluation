<?php

class ElectronicItems
{
	/**
	 * List of items in the cart.
	 *
	 * @var array
	 */
	private $items = array();

	/**
	 * Total amount.
	 *
	 * @var float
	 */
	private $totalAmount;

	/**
	 * List items in the cart.
	 *
	 * @return array
	 */
	public function getItems(): array
	{
		return $this->items;
	}

	/**
	 * Add an electronic item to the cart.
	 *
	 * @param ElectronicItem $item
	 */
	public function addItem(ElectronicItem $item) {
		$this->items[] = $item;
		$this->totalAmount += $item->getPrice();
	}

	/**
	 * Empty cart and reset its total amount.
	 */
	public function emptyItems() {
		$this->items = array();
		$this->totalAmount = 0;
	}

	/**
	 * Private method used to sort items by price
	 *
	 * @param ElectronicItem $a
	 * @param ElectronicItem $b
	 *
	 * @return bool
	 */
	private function cmp(ElectronicItem $a, ElectronicItem $b) {
		return $a->getPrice() > $b->getPrice();
	}


	/**
	 * Returns the items, sorted by price.
	 *
	 * @return array
	 */
	public function getSortedItems() {
		$sorted = $this->items;
		usort($sorted, array($this, "cmp"));
		return $sorted;
	}

	/**
	 * Return cart total amount.
	 *
	 * @return float
	 */
	public function getTotalAmount() {
		return $this->totalAmount;
	}

	/**
	 * Returns the items depending on the sorting type requested
	 *
	 * @param string $type
	 * @return array
	 */
	public function getItemsByType(string $type) {

		// Validate asked type.
		if (in_array($type, ElectronicItem::$types)) {
			$callback = function($item) use ($type) {
				return $item->getType() == $type;
			};
			return array_filter($this->items, $callback);
		}

		// Else, return empty.
		return array();
	}
}
