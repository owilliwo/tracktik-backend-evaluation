<?php

use ElectronicItem;

class Microwave extends ElectronicItem
{
	/**
	 * Microwave constructor.
	 *
	 * @param string $name
	 * @param float $price
	 */
	public function __construct(string $name, float $price) {
		$this->setType(ElectronicItem::ELECTRONIC_ITEM_MICROWAVE);
		$this->setName($name);
		$this->setMaximumExtras(0);
		$this->setPrice($price);
	}
}
