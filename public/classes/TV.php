<?php

use ElectronicItem;

class TV extends ElectronicItem
{
	/**
	 * TV constructor.
	 *
	 * @param string $name
	 * @param float $price
	 */
	public function __construct(string $name, float $price) {
		$this->setType(ElectronicItem::ELECTRONIC_ITEM_TELEVISION);
		$this->setName($name);
		$this->setMaximumExtras(null);
		$this->setPrice($price);
	}
}
