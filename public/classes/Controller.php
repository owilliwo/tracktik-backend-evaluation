<?php

use ElectronicItem;

class Controller extends ElectronicItem
{
	/**
	 * @var bool
	 */
	public $wired;

	/**
	 * Controller constructor.
	 *
	 * @param string $name
	 * @param float $price
	 * @param bool $wired
	 */
	public function __construct(string $name, float $price, bool $wired = false) {
		$this->setType(ElectronicItem::ELECTRONIC_ITEM_CONTROLLER);
		$this->setName($name);
		$this->setPrice($price);
		$this->setMaximumExtras(0);
		$this->setWired($wired);
	}

	function getWired() {
		return $this->wired;
	}

	function setWired($wired) {
		$this->wired = $wired;
	}
}
