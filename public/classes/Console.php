<?php

use ElectronicItem;

class Console extends ElectronicItem
{
	/**
	 * Console constructor.
	 *
	 * @param string $name
	 * @param float $price
	 */
	public function __construct(string $name, float $price) {
		$this->setType(ElectronicItem::ELECTRONIC_ITEM_CONSOLE);
		$this->setName($name);
		$this->setMaximumExtras(4);
		$this->setPrice($price);
	}
}
