<?php

class ElectronicItem
{
	/**
	 * Item name.
	 *
	 * @var string
	 */
	public $name;

	/**
	 * Item price.
	 *
	 * @var float
	 */
	public $price;

	/**
	 * Item type.
	 *
	 * @var string
	 */
	private $type;

	/**
	 * Item maximum extras (attached items).
	 * Null for no-limit, 0 or more if there is a maximum.
	 *
	 * @var int|null
	 */
	private $maximumExtras;

	/**
	 * Item extras (attached items).
	 *
	 * @var array
	 */
	private $extras = array();

	/**
	 * Allowed types for ElectronicItem object.
	 */
	const ELECTRONIC_ITEM_TELEVISION = 'television';
	const ELECTRONIC_ITEM_CONSOLE = 'console';
	const ELECTRONIC_ITEM_MICROWAVE = 'microwave';
	const ELECTRONIC_ITEM_CONTROLLER = 'controller';
	public static $types = array(
		self::ELECTRONIC_ITEM_CONSOLE,
		self::ELECTRONIC_ITEM_MICROWAVE,
		self::ELECTRONIC_ITEM_TELEVISION,
		self::ELECTRONIC_ITEM_CONTROLLER
	);

	/**
	 * Magic method to render an ElectronicItem object as a string.
	 *
	 * @return string
	 */
	public function __toString() {
		return get_class($this) . ' ' . $this->name . ': $' . $this->price;
	}

	/**
	 * MEMBERS FUNCTIONS.
	 */

	function getName() {
		return $this->name;
	}

	function getPrice() {
		return $this->price;
	}

	function getType() {
		return $this->type;
	}

	function getMaximumExtras() {
		return $this->maximumExtras;
	}

	function getExtras() {
		return $this->extras;
	}

	function setName($name) {
		$this->name = $name;
	}

	function setPrice($price) {
		$this->price = $price;
	}

	function setType($type) {
		$this->type = $type;
	}

	function setMaximumExtras($maximumExtras) {

		// Make sure not having a negative value before setting it.
		$maximumExtras = ($maximumExtras >= 0) ? $maximumExtras : 0;
		$this->maximumExtras = $maximumExtras;
	}

	/**
	 * Add an extra to the item.
	 *
	 * @param Controller $extra
	 * @throws Exception
	 */
	public function addExtra(Controller $extra) {
		if ($this->maximumExtras === null || count($this->extras) < $this->maximumExtras) {
			$this->extras[] = $extra;
		}
		else {
			throw new Exception('This item can not have more than ' . $this->maximumExtras . ' extra(s).');
		}
	}

	/**
	 * Remove all extras from an item.
	 */
	public function emptyExtras() {
		$this->extras = array();
	}

	/**
	 * Return total item price (including extras prices).
	 */
	public function getTotalPrice() {
		$totalPrice = $this->price;
		foreach ($this->extras as $extra) {
			$totalPrice += $extra->price;
		}
		return $totalPrice;
	}
}
