<?php

// Basic autoloader.
function ttk_autoloader($class) {
	include 'classes/' . $class . '.php';
}

spl_autoload_register('ttk_autoloader');
